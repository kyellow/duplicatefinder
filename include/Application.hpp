/* Application.hpp
 *
 * Copyright (C) 2016 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <ProgramOptions.hpp>

/// Define class to
class Application
{
	constexpr auto& VERSION = "0.1.1";

	private: 
		 // Option values that handles command line
		 std::string _dirpath;
		 
	public:
		/// Default constructor.
		Application() : {};
		Application( Application&& ) = default;
		Application& operator=( Application&& ) = default;

		/// Parse the command-line to start the application processes and prepare to handle the file.
		bool init(int argc, char* argv[])
		{
			bool help;
			bool version;

			opt::ProgramOptions po;
			po.variable(_dirpath,'D',"dirpath",std::string(""),"Directory @TODO");

			if( !po.parse(argc,argv) || help)
			{
				std::cout << " Try duplicatefinder --help for more information. " << std::endl;
				return false;
			}

			if (help)
			{
				po.printHelp(argv[0]);
				return false;
			}

			if( version )
			{
				std::cout << "duplicatefinder " << VERSION << std::endl;
				return false;
			}

			return true;
		}

		/// Parses the file and @TODO
		void run()
		{
			
		}
	
		/// Destructor
		virtual ~Application()
		{
			
		}
};

#endif
