/* ProgramOptions.hpp
 * Program Options Class that parse command-line parameters
 *
 * Copyright (C) 2016 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROGRAM_OPTIONS_HPP
#define PROGRAM_OPTIONS_HPP

// http://pubs.opengroup.org/onlinepubs/000095399/functions/getopt.html
#include <getopt.h>

#include <string>
#include <functional>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <sstream>

namespace opt
{

// Program Options Class that parse command-line parameters
class ProgramOptions {

	public:
		ProgramOptions() : id(256) { }

		template <typename T>
		void variable(T & var, char shortFlag, std::string longFlag, T defaultValue, std::string description)
		{
			struct option op;
			this->entry(op, shortFlag, longFlag, defaultValue, description);

			if(std::is_same<T, bool>::value)
			{
				op.has_arg = no_argument;
				var = false;
				this->setters[op.val] = [&var](std::string) { var = true; };
			}
			else
			{
				this->optionStr += ":";
				op.has_arg = required_argument;
				var = defaultValue;
				this->setters[op.val] = std::bind(&ProgramOptions::set<T>, this, std::ref(var), std::placeholders::_1);
			}

			this->options.push_back(op);
		}

		bool parse(int argc, char ** argv)
		{
			this->options.push_back({NULL, 0, NULL, 0});
			int ch;

			while ((ch = getopt_long(argc, argv, this->optionStr.c_str(), &this->options[0], NULL)) != -1) 
			{
				auto it = this->setters.find(ch);

				if (it != this->setters.end()) 
				{
					if (optarg) 
						it->second(optarg);
					else 
						it->second("");
				}
				else
					return false;
			}
			return true;
		}

		void printHelp(char * argv0, std::ostream & to = std::cout)
		{
			to << "Usage: " << argv0 << " [options]" << std::endl <<std::endl;
			to << this->help << std::endl;
		}

	private:
		int id;		// shortflag default val

		std::unordered_map<int, std::function< void(std::string optarg) > > setters; // flag id -> setters
		std::unordered_set<std::string> longFlags;	// long version of option
		std::string help; 
		std::vector<struct option> options;		// vector is sequential in memory, so we can use it as argv param of getopt 
		std::string optionStr;	// as argc param of getopt

		template <typename T>
		void set(T & var, std::string optarg) 
		{ 
			std::stringstream ss(optarg); 
			ss >> var; 
		}

		template <typename T>
		void entry(struct option & op, char shortFlag, std::string longFlag, T & defaultValue, std::string description)
		{
			if (!shortFlag && !longFlag.size()) 
				throw std::string("no flag specified");

			if (shortFlag) 
			{
				if (this->setters.find((int)(shortFlag)) != this->setters.end()) 
					throw std::string("Short flag exists: ") + shortFlag;

				this->optionStr += shortFlag;
				op.val = shortFlag;
			} 
			else 
				op.val = this->id++;

			if (longFlag.size()) 
			{
				if (this->longFlags.find(longFlag) != this->longFlags.end()) 
					throw std::string("long flag exists: ") + longFlag;

				op.name = this->longFlags.insert(longFlag).first->c_str();
			}

			op.flag = NULL;

			// generate help item
			std::stringstream ss;
			ss << "  ";

			if (shortFlag) 
				ss << "-" << shortFlag << " ";
			
			if (longFlag.size()) 
				ss << "--" << longFlag << " ";
			
			if(!std::is_same<T, bool>::value && defaultValue != T())
				ss << "[default: " << defaultValue << "]";

			ss << std::endl;

			constexpr size_t step = 80 - 6;		// terminal width character limit 

			for (size_t i = 0; i < description.size(); i += step) 
			{
				ss << "      ";
				if (i + step < description.size()) 
					ss << description.substr(i, step) << std::endl;
				else
					ss << description.substr(i);
			}
			
			ss << std::endl;

			this->help.append(ss.str());
		}
};


}	// end opt namespace

#endif
