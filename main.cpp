/* main.cpp
 * DUPLICATEFINDER finds duplicate files within specified directories
 *
 * Copyright (C) 2016 kyellow
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Application.hpp>
#include <time.h>

int main(int ac, char* av[])
{
	Application app;

	if( app.init(ac,av) )
	{
		struct timespec start,end;
		clock_gettime(CLOCK_MONOTONIC,&start);

		app.run();

		clock_gettime(CLOCK_MONOTONIC,&end);

		auto timeDiff = [](timespec& s, timespec& e) -> time_t { 
			if( (e.tv_nsec-s.tv_nsec) < 0 ) 
				return (e.tv_sec-s.tv_sec-1);
			else 
				return (e.tv_nsec-s.tv_nsec);
		};

		app.printFrequency();

		std::cout<<" Processing Time as Sec : "<< timeDiff(start,end)<<std::endl;
	}
	
	return 0;	
}
