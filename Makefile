C++ = g++

ifndef os
   os = LINUX
endif

ifndef arch
   arch = IA64
endif

CCFLAGS = -std=c++1y -Wall -D$(os) -I./include -finline-functions -O3

ifeq ($(arch), IA32)
   CCFLAGS += -DIA32 #-mcpu=pentiumpro -march=pentiumpro -mmmx -msse
endif

ifeq ($(arch), POWERPC)
   CCFLAGS += -mcpu=powerpc
endif

ifeq ($(arch), IA64)
   CCFLAGS += -DIA64
endif

ifeq ($(arch), SPARC)
   CCFLAGS += -DSPARC
endif

LDFLAGS = -L ./lib -static -lstdc++ -lm

#-lpthread -lm

#ifeq ($(os), UNIX)
#   LDFLAGS += -lsocket
#endif

#ifeq ($(os), SUNOS)
#   LDFLAGS += -lrt -lsocket
#endif

DIR = $(shell pwd)

APP = main 

all: $(APP)

%.o: %.cpp
	$(C++) $(CCFLAGS) $< -c

main: main.o
	$(C++) $^ -o $@ $(LDFLAGS)

clean:
	rm -f *.o $(APP)

install:
	export PATH=$(DIR):$$PATH
